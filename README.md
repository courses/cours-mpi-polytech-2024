# MPI cours 2024 Polytech

It is a revealjs presentation but revealsjs is not included here.

To serve the presentation, launch the bash script :

```bash
deploy.sh help

/deploy.sh [option] : create and serve a js presentation

   Options:
           build : to clone the revealjs, update and deploy
           link  : update the deployed site when modifing an of the local files
           serve : to serve the web site only

```