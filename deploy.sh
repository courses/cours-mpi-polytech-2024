#!/bin/bash
#set -x


Help(){
    echo "$0 [option] : create and serve a js presentation"
    echo
    echo "   Options:"
    echo "           build : to clone the revealjs, update and deploy"
    echo "           link  : update the deployed site when modifing an of the local files "
    echo "           serve : to serve the web site only"


}

Serve(){
    echo "Serving the presentation"
    cd build
    npm start
}

Link(){
    echo "Relinking reveals"
    cd build
    rsync ../MPIcours/css/MPIstyle.css css/
    rsync ../MPIcours/js/MPIjs.js js/
    rsync -az ../MPIcours/lib .
    ln -sf ../MPIcours/markdown .
    ln -sf ../MPIcours/figures .
    ln -sf ../cours.html index.html
}


Build (){
    echo "Installing Reveal.js"
    git clone https://github.com/hakimel/reveal.js.git build
    cd build
    npm install
    npm install reveal.js-menu
    cd ..
    Link
}

if [[ $1 == 'help' ]];then
    Help
    exit 0
fi

if [[ ! -d "build" ]]; then
    Build
    exit 0
fi

if [[ $1 == 'build' ]];then
    Build
    exit 0
fi

if [[ $1 == 'link' ]];then
    Link
    exit 0
fi


Serve
