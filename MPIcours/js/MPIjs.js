'use strict';

(function(exports){

var titulo = "MMancini MPI-2024";
var html_barra = '<div id="footer-left"><h6 class="titulo">' + titulo +
'</h6></div>';
if ( window.location.search.match( /print-pdf/gi ) ) {
  $('section').append(html_barra);
}
else {
  $('div.reveal').append(html_barra);
}

function set_md(){
  //get all elements of Markdown and set them the attributes
  document.getElementsByClassName('md').forEach((item, i) => {
    item.setAttribute("data-separator","^\n\n\n")
    item.setAttribute("data-separator-vertical","^\n\n")
    item.setAttribute("data-separator-notes","^Note:")
    item.setAttribute("data-charset","iso-8859-15")
    //console.log(item);
  });

}


function set_section(){

  var titulo = $("section.present").find("p.chapter").text();
  var topleft = $("#topleft");
  console.log("lll",titulo,topleft);

  topleft.remove();
  var html_barra = '<div id="topleft"><h6 class="titulo">' + titulo +
  '</h6></div>';
  if ( window.location.search.match( /print-pdf/gi ) ) {
    $('section').append(html_barra);
  }
  else {
    $('div.reveal').append(html_barra);
  }
}


function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '38' ||
      e.keyCode == '40' ||
      e.keyCode == '37' ||
      e.keyCode == '39'
    ) {
      console.log("KEY",e.keyCode);
      set_section();
    }
}

window.onload = function(){
  set_md();
  console.log("reload");

  // set_section();
  //
  // $(".navigate-right.enabled").click(function(){
  //   console.log("RIGHT");
  //   set_section();
  // });
  // $(".navigate-left.enabled").click(function(){
  //   console.log("LEFT");
  //   set_section();
  //  });
  //
  // document.addEventListener('keydown', function(e) {
  // switch (e.keyCode) {
  //  case 37:
  //      set_section();
  //      break;
  //  case 38:
  //     set_section();
  //      break;
  //  case 39:
  //      set_section();
  //      break;
  //  case 40:
  //      set_section();
  //      break;
  // }
  // });
};

  //document.onkeydown = checkKey;

  // exports.set_section = set_section;

})(this.myClass = {});
