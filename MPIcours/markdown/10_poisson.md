## Small Project

### TP 8 : Poisson's equation

Resolution of the following Poisson equation :
$$
\displaylines{
  \frac{\partial^2{u}}{\partial{x^2}} + \frac{\partial^2{u}}{\partial{y^2}} &amp; = f(x,y) in [0,1]\times[0,1] \\\ u(x,y) = 0 &amp; \textrm{on the boundaries} \\\f(x,y) &amp; = 2\cdot(x^2-x+y^2-y)
}$$

 We will solve this equation with a domain decomposition method :

- The equation is discretized on the domain with a finite difference method.
- The obtained system is resolved with a Jacobi solver.
- The global domain is split into sub-domains.

The exact solution is known and is $uexact(x,y) = xy(x−1) (y−1)$.



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

To discretize the equation, we define a grid with a set of `points(xi,yj)`.

- $x_i = i\cdot h_x $  `for i=0,... ,ntx+1`
- $y_j = j\cdot h_y $  `for j=0,... ,nty+1`
- $h_x = \frac{1}{ntx+1}$  :  x-wise step
- $h_y =\frac{1}{nty+1}$  : y-wise step
- $ntx$ : number of x-wise `interior` points
- $nty$ : number of y-wise `interior` points.

In total, there are `ntx+2` points in the x direction and `nty+2` points in the y direction.



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

- Let $u_{ij}$ be the estimated solution at position $x_i = i\cdot h_x$ and $x_j=j\cdot h_y$.
- The Jacobi solver consist of computing :

$$
u_{ij}^{n+1} = C_0 (C_1(u_{i+1j}^n+u_{i-1 j}^n)
                 + (C_2(u_{ij+1}^n+u_{ij-1}^n))-f_{ij})
$$
with:

$$\displaylines{
C_0 &amp; =  \frac{1}{2}\frac{h_x^2h_y^2}{h_x^2+h_y^2} \\\ C_1 &amp; = \frac{1}{h_x^2} \\\ C_2 &amp; = \frac{1}{h_y^2}
}
$$



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

- In parallel, the interface values of subdomains must be exchanged between the neighbours.
- We use ghost cells as receive buffers.

<img src="../figures/ghost.png" width="50%" ></img>


-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

<img src="../figures/numerization.png" width="80%" ></img>



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

Process rank numbering in the sub-domains

<img src="../figures/process_rank.png" width="50%" ></img>



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

<img src="../figures/4domains.png" width="40%" ></img>

<h3 class="left2">You don't need to do :</h3>

- Define a view, to see only the owned part of the global matrix u ;
- Define a type, in order to write the local part of matrix u(without interfaces) ;
- Apply the view to the file ;
- Write using only one call.



-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation

- Initialisation of the MPI environment.
- Creation of the 2D Cartesian topology.
- Determination of the array indexes for each sub-domain.
- Determination of the 4 neighbour processes for each sub-domain.
- Creation of two derived datatypes,type_line and type_column.- Exchange the values on the interfaces with the other sub-domains.
- Computation of the global error. When the global error is lower than a specified value (machine precision for example), we consider that we have reached the exact solution.
- Collecting of the global matrix u (the same one as we obtained in the sequential) in an MPI-IO file data.dat (DONE).


-----
<p class="chapter">Small project</p>

### TP 8 : Poisson's equation
- A skeleton of the parallel version is proposed : It consists of a main program (poisson.c) and several subroutines. All the modifications have to be done in the module_parallel.c file.
- To compile and execute the code, use make and to verify the results, use make verification which runs a reading program of the data.dat file and compares it with the sequential version.


-----
<p class="chapter">Small project</p>

### TP 9

Create a 2-dimensional cartesian grid topology to communicate between processes.
- In each processus a variable is initialized to the local rank of the cartesian communicator.

The exercise is divided in three steps:

1. Compare the local rank with the global `MPI_COMM_WORLD` rank. Are they the same number?

2. On each task, calculate the average between its local rank and the local rank from each of its neighbours (north, east, south, west). Notice that in order to do this the cartesian communicator has to be periodic (the bottom rank is neighbour of the top)

3. Calculate the average of the local ranks on each row and column. Create a family of sub-cartesian communicators to allow the communications between rows and columns.
