# Derived Types



-----
<p class="chapter">Derived types</p>

## Derived Types

- In communications, exchanged data have different datatypes : `MPI_INT`, `MPI_FLOAT`, etc.
- We can create more complex data structures by using functions such as `MPI_Type_contiguous`, `MPI_Type_vector`, `MPI_Type_indexed` or `MPI_Type_create_struct`.
- Derived datatypes allow exchanging non-contiguous or non-homogenous data in the memory and limiting the number of calls to communications subroutines.



-----
<p class="chapter">Derived types</p>

## Hierarchy of Types Constructors

`MPI_Type_create_struct`

`MPI_Type_[create_h]indexed`

`MPI_Type_[create_h]vector`

`MPI_Type_contiguous`

`MPI_FLOAT`, `MPI_INT`, `MPI_DOUBLE`



-----
<p class="chapter">Derived types</p>

### Contiguous Datatypes
`MPI_Type_contiguous` creates a data structure from a homogenous set of existing datatypes  <span class="myred"> **contiguous in memory**</span>.

<img src="../figures/contiguous.png" width="30%" ></img>

```C
int MPI_Type_contiguous(  int count,  MPI_Datatype old_type,  MPI_Datatype *new_type_p);

```


-----
<p class="chapter">Derived types</p>

### Constant stride
`MPI_Type_vector` creates a data structure from a homogenous set of existing datatypes <span class="myred"> **separated by a constant stride**</span> in memory. The stride is given in number of elements.

<figure class="image">
  <img width="30%"  src="../figures/stride.png" alt="{{ stride }}" style="margin-bottom:-5px">
  <figcaption style="margin-bottom:40px">MPI_Type_vector(5,1,7,MPI_FLOAT,mystride)</figcaption>
</figure>

```C
  int MPI_Type_vector(int count,  int blocklength,  int stride,  MPI_Datatype old_type,  MPI_Datatype *newtype_p);
```


-----
<p class="chapter">Derived types</p>

### Constant stride
- `MPI_Type_create_hvector` creates a data structure from a homogenous set of existing datatypes <span class="myred"> **separated by a constant stride**</span> in memory. The stride is given in <span class="myred">**bytes**</span>.

- This call is useful when the old type is no longer a base datatype (`MPI_INT`, `MPI_FLOAT`,...) but a more complex datatype constructed by using MPI functions, because in this case the stride can no longer be given in number of elements.

```C
  int MPI_Type_create_hvector(int count,  int blocklength,  int stride (in bytes),
                              MPI_Datatype  old_type,
                              MPI_Datatype *newtype_p);
```

-----
<p class="chapter">Derived types</p>

### Commit and Free

Before using a new derived datatype, it is necessary to validate it with the `MPI_Type_commit` function.
```C
int MPI_Type_commit(
        MPI_Datatype *datatype
);
```

- The freeing of a derived datatype is made by using the `MPI_Type_free` function.
```C
int MPI_Type_free(
        MPI_Datatype *datatype
);
```

-----
<p class="chapter">Derived types</p>

### Exercice 3.8 : Contiguous

Use the `MPI_Type_contiguous` to send the first line of a matrix on processus 0 to the last line of the same matrix on the processus 1.


-----
<p class="chapter">Derived types</p>

### Exercice 3.9 : Stride

Use the `MPI_Type_vector` to send the first column of a matrix on processus 0 to the last coloumn of the same matrix on the processus 1.

-----
<p class="chapter">Derived types</p>

### Exercice 3.95 : Block

Use the `MPI_Type_vector` to send the a block of a matrix on processus 0 to the the end of the same matrix on the processus 1.



-----
<p class="chapter">Derived types</p>

### Homogenous datatypes of variable strides

- `MPI_Type_indexed` allows to create a data structure composed of a sequence of blocks containing a variable number of elements separated by a variable stride in memory. The stride is given in number of elements.
- `MPI_Type_create_hindexed` has the same functionality as `MPI_Type_indexed` except that the strides separating two data blocks are given in bytes.
This subroutine is useful when the old datatype is not an MPI base datatype (`MPI_INT`,`MPI_FLOAT`, ...). We cannot therefore give the stride in number of elements of the old datatype.
- For `MPI_Type_create_hindexed`(), as for `MPI_Type_create_hvector`,use `MPI_Type_size` or `MPI_Type_get_extent` in order to obtain in a portable way the size of the stride in bytes.


-----
<p class="chapter">Derived types</p>

### MPI_Type_indexed

<img src="../figures/new_type.png" width="40%" ></img>

```C
int MPI_Type_indexed(
  int nb,
  int block_length[],
  int displacements[],  //  Attention the displacements are given in elements of the old_type
  MPI_Datatype old_type,
  MPI_Datatype *newtype
);
```

-----
<p class="chapter">Derived types</p>

### MPI_Type_create_hindexed

<img src="../figures/new_type_indexed.png" width="40%" ></img>

```C
int MPI_Type_create_hindexed(
  int nb,
  int block_length[],
  int displacements[],  //  Attention the displacements are given in bytes
  MPI_Datatype old_type,
  MPI_Datatype *newtype
);
```

-----
<p class="chapter">Derived types</p>

### Exercice 3.97 : Triangular matrix

In the following example, each of the two processes :
1. Initializes its matrix (positive growing numbers on process 0 and negative decreasing numbers on process 1).
2. Constructs its datatype : triangular matrix (superior for the process 0 and inferior for the process 1).
3. Sends its triangular matrix to the other process and receives back a triangular matrix which it stores in the same place which was occupied by the sent matrix. This is done with the `MPI_Sendrecv_replace` subroutine.
4. Frees its resources and exits MPI.



-----
<p class="chapter">Derived types</p>

### Exercice 3.97 : Triangular matrix


<div style="font-size:0.5em;" class="box" id="part-1">
<h3> before communication</h3>

rank: 0

| ||||||||
|:---|----|----|----|----|----|----|----|
|  0 |  1 |  2 |  3 |  4 |  5 |  6 |  7 |
|  8 |  9 | 10 | 11 | 12 | 13 | 14 | 15 |
| 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 |
| 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 |
| 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 |
| 40 | 41 | 42 | 43 | 44 | 45 | 46 | 47 |
| 48 | 49 | 50 | 51 | 52 | 53 | 54 | 55 |
| 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 |

rank: 1

| ||||||||
|:---|----|----|----|----|----|----|----|
|  0 | -1 | -2 | -3 | -4 | -5 | -6 | -7 |
| -8 | -9 |-10 |-11 |-12 |-13 |-14 |-15 |
|-16 |-17 |-18 |-19 |-20 |-21 |-22 |-23 |
|-24 |-25 |-26 |-27 |-28 |-29 |-30 |-31 |
|-32 |-33 |-34 |-35 |-36 |-37 |-38 |-39 |
|-40 |-41 |-42 |-43 |-44 |-45 |-46 |-47 |
|-48 |-49 |-50 |-51 |-52 |-53 |-54 |-55 |
|-56 |-57 |-58 |-59 |-60 |-61 |-62 |-63 |

 </div>
<div class="box" style="font-size:0.5em;" id="part-2">
<h3> after communication</h3>

rank: 0

| ||||||||
|:---|----|----|----|----|----|----|----|
|  0 | -8 |-16 |-17 |-24 |-25 |-26 |-32 |
|  8 |  9 |-33 |-34 |-35 |-40 |-41 |-42 |
| 16 | 17 | 18 |-43 |-44 |-48 |-49 |-50 |
| 24 | 25 | 26 | 27 |-51 |-52 |-53 |-56 |
| 32 | 33 | 34 | 35 | 36 |-57 |-58 |-59 |
| 40 | 41 | 42 | 43 | 44 | 45 |-60 |-61 |
| 48 | 49 | 50 | 51 | 52 | 53 | 54 |-62 |
| 56 | 57 | 58 | 59 | 60 | 61 | 62 | 63 |

rank: 1

| ||||||||
|:---|----|----|----|----|----|----|----|
|  0 | -1 | -2 | -3 | -4 | -5 | -6 | -7 |
|  1 | -9 |-10 |-11 |-12 |-13 |-14 |-15 |
|  2 |  3 |-18 |-19 |-20 |-21 |-22 |-23 |
|  4 |  5 |  6 |-27 |-28 |-29 |-30 |-31 |
|  7 | 10 | 11 | 12 |-36 |-37 |-38 |-39 |
| 13 | 14 | 15 | 19 | 20 |-45 |-46 |-47 |
| 21 | 22 | 23 | 28 | 29 | 30 |-54 |-55 |
| 31 | 37 | 38 | 39 | 46 | 47 | 55 |-63 |

</div>



-----
<p class="chapter">Derived types</p>

### Size of Datatypes

- `MPI_Type_size` returns the number of bytes needed to send a datatype. This value ignores any holes present in the datatype.

```C
int MPI_Type_size(
  MPI_Datatype datatype,
  int *size
);
```

- The extent of a datatype is the memory space occupied by this datatype (in bytes).
This value is used to calculate the position of the next datatype element (i.e. the
stride between two successive datatype elements).

```C
int MPI_Type_get_extent(
  MPI_Datatype datatype,
  MPI_Aint *lb,
  MPI_Aint *extent
);
```



-----
<p class="chapter">Derived types</p>

### the EXTENT

- The extent is a datatype parameter.
- By default, it’s the space in memory between the first and last component of a datatype (bounds included and with alignment considerations).
- We can modify the extent to create a new datatype by adapting the preceding one using `MPI_Type_create_resized`.
- This provides a way to choose the stride between two successive datatype elements.

```C
int MPI_Type_create_resized(
  MPI_Datatype oldtype,
  MPI_Aint lb,
  MPI_Aint extent,
  MPI_Datatype *newtype
);
```



-----
<p class="chapter">Derived types</p>

<img src="../figures/extend_1.png" width="50%" ></img>



-----
<p class="chapter">Derived types</p>

<img src="../figures/extend_2.png" width="50%" ></img>



-----
<p class="chapter">Derived types</p>

<img src="../figures/extend_3.png" width="40%" ></img>



-----
<p class="chapter">Derived types</p>

### Heterogenous datatype

- `MPI_Type_create_struct` allows creating a set of data blocks indicating the type,
the count and the displacement of each block.
- It is the most general datatype constructor. It further generalizes `MPI_Type_indexed` by
allowing a different datatype for each block.

<img src="../figures/create_struc.png" width="70%" ></img>

```C
int MPI_Type_create_struct(
  int count,
  int array_of_blocklengths[],
  MPI_Aint array_of_displacements[],
  MPI_Datatype array_of_types[],
  MPI_Datatype *newtype
);

```




-----
<p class="chapter">Derived types</p>

### Compute displacements

- `MPI_Type_create_struct` is useful for creating MPI datatypes corresponding to C structures.
- The memory alignment of heterogeneous data structures is different for each architecture and each compiler.
- The displacement between two components of a C structure can be obtained by calculating the difference between their memory addresses.
- `MPI_Get_address` provides the address of a variable. It’s equivalent of `&` operator in C.
- **Warning**, even in C, it is better to use this subroutine for portability reasons.
- **Warning**, you have to check the extent of the MPI datatypes obtaineds.

```C
int MPI_Get_address(
  void *location,
  MPI_Aint *address
);
```


-----
<p class="chapter">Derived types</p>

### example  16 : Derived type

```C
typedef struct{
    char category[5];
    int mass;
    float coords[3];
    int class;
}  Particles ;

int main(int argc, char* argv[])
{
    int n = 1000;
    int my_rank, comm_size;

    Particles p[n], temp_p[n];
    MPI_Datatype types[] = {MPI_CHAR,MPI_INT,MPI_FLOAT,MPI_INT};
    int block_lengths[] = {5,1,3,1};
    MPI_Datatype temp, type_particles;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    MPI_Aint addresses[4];
    MPI_Get_address(&p[0].category,&(addresses[0]));
    MPI_Get_address(&p[0].mass,&addresses[1]);
    MPI_Get_address(&p[0].coords,&addresses[2]);
    MPI_Get_address(&p[0].class,&addresses[3]);

    MPI_Aint displacement[4];
```

-----
<p class="chapter">Derived types</p>


```C
    for(int i=0;i<4;i++){	displacement[i] = addresses[i]-addresses[0];}

    /* MPI_Aint displacement[] = {0,4*sizeof(char), */
    /* 		    4*sizeof(char)+sizeof(int), */
    /* 		    4*sizeof(char)+sizeof(int)+3*sizeof(float), */
    /* }; */

    MPI_Type_create_struct(4,block_lengths,displacement, types, &type_particles);
    MPI_Type_commit(&type_particles);

    if(my_rank == 0) {
    	strncpy(p[0].category, "helo", 4);
    	p[0].mass = 3; 	p[0].coords[0] = 1.;	p[0].coords[1] = 1.;	p[0].coords[2] = 1.;
    	p[0].class = 5;
    	printf("proc 0 : %s %d %f %d\n",p[0].category,p[0].mass,p[0].coords[0],p[0].class);

    	MPI_Send(&(p[0]),n-1,type_particles,1,100,MPI_COMM_WORLD);
    }
    else{
    	MPI_Recv(&p[1],n-1,type_particles,0,100,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    	printf("proc 1 : %s %d %f %d\n",p[1].category,p[1].mass,p[1].coords[0],p[1].class);
    }

    MPI_Type_free(type_particle);
    MPI_Finalize();
    return 0;
}

```


-----
<p class="chapter">Derived types</p>

### TP 4 : Transpose of a matrix

- The goal of this exercise is to practice with the derived datatypes.
- `A` is a matrix with 5 lines and 4 columns defined on the process 0.
- Process 0 sends its A matrix to process 1 and transposes this matrix during the send.

<img src="../figures/transpose.png" width="50%" ></img>

- To do this, we need to create two derived datatypes, a derived datatype type_line and a derived datatype type_transpose.


-----
<p class="chapter">Derived types</p>

## TP 5 : Matrix-matrix product

- Collective communications : matrix-matrix product `C=A×B`
- The matrixes are square and their sizes are a multiple of the number of processes.
- The matrixes `A` and `B` are defined on process 0. Process 0 sends a horizontal slice of matrix `A` and a vertical slice of matrix `B` to each process. Each process then calculates its diagonal block of matrix `C`.
- To calculate the non-diagonal blocks, each process sends to the other processes its own slice of A.
- At the end, process 0 gathers and verifies.



-----
<p class="chapter">Derived types</p>

## TP 5 : Matrix-matrix product

<img src="../figures/matrix_dot.png" width="50%" ></img>
