# Communication Modes


---
<p class="chapter">Communication Modes</p>


<h3 class="left2">Point-to-Point Send Modes</h3>

| Mode|Blocking  | Non-blocking |
|-----|:--------:|-------------:|
| Standard send | MPI_Send| MPI_Isend |
| Synchronous send | MPI_Ssend | MPI_Issend |
| Buffered send| MPI_Bsend |MPI_Ibsend|
|||
| Receive | MPI_Recv|MPI_Irecv|


---
<p class="chapter">Communication Modes</p>


<h3 class="left">Blocking call</h3>


- A call is blocking if the memory space used for the communication can be reused immediately after the exit of the call.
- The data sent can be modified after the call.
- The data received can be read after the call.


---
<p class="chapter">Communication Modes</p>


<h3 class="left">Synchronous sends</h3>

A synchronous send involves a synchronization between the involved processes. A send cannot start until its receive is posted.

There can be no communication before the two processes are ready to communicate.

<div class="box" id="part-1">
<hr>
<h3> Rendezvous Protocol</h3>

 The rendez vous protocol is generally  the  protocol  used  for  synchronous  sends  (implementation-dependent).
 The  return  receipt  is optional.
 </div>
<div class="box" id="part-2">
<img src="../figures/send_recv_blocking.png" width="100%" style="justify-content: center"></img>
</div>


---
<p class="chapter">Communication Modes</p>

### Interface of  `MPI_Ssend`

```C
int MPI_Ssend(
  void *buf, int count,
  MPI_Datatype datatype,
  int dest,int tag,
  MPI_Comm comm
);

```

- ### Advantages:
  -  Low resource consumption (no buffer)
  -  Rapid if the receiver is ready (no copying in a buffer)
  - Knowledge of receipt through synchronization

- ### Disadvantages:
  - Waiting time if the receiver is not there/not ready
  - Risk of deadlocks


---
<p class="chapter">Communication Modes</p>

### Exemple of Deadlock

```C
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, value, result, orank, tag=110;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // only 2 procs
    orank = (rank+1)%2;

    value = rank+1000;

    MPI_Ssend(&value,1,MPI_INT,orank,tag,MPI_COMM_WORLD);

    MPI_Recv(&result,1,MPI_INT,orank,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    MPI_Finalize();
    return 0;
}
```

In this example, there is a deadlock because we are in synchronous mode. The two processes are blocked on the `MPI_SSEND` call because they are waiting for the `MPI_RECV` of the other process. However, the `MPI_RECV` call can only be made after the unblocking of the `MPI_SSEND` call.


---
<p class="chapter">Communication Modes</p>

<h3 class="left2">Buffered sends</h3>

A buffered send implies the copying of data into an intermediate memory space. There is then no coupling between the two processes of communication. Therefore, the return of this type of send does not mean that the receive has occurred.

### Protocol with user buffer on the sender side
<div class="box" id="part-1">
In  this  approach,  the  buffer  is  on the  sender  side  and  is  managed explicitly by the application.

A buffer managed by MPI can exist on the  receiver  side.

Many variants are possible.

The return receipt is optional.

 </div>
<div class="box" id="part-2">
<img src="../figures/buffered_send.png" width="100%" style="justify-content: center"></img>
</div>


---
<p class="chapter">Communication Modes</p>

<h3 class="left2">Buffered sends</h3>

The buffers have to be managed manually (with calls to `MPI_Buffer_attach` and `MPI_Buffer_detach`). Message header size needs to be taken into account when allocating buffers (by adding the constant `MPI_Bsend_overhead` for each message occurrence).

```C
int MPI_Buffer_attach(  void *buf,  int size);
int MPI_Bsend(void *buf,  int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm
);
int MPI_Buffer_detach(  void *buf, int *size);
);
```

---
<p class="chapter">Communication Modes</p>

### `MPI_Ssend`

- ### Advantages:
  - Often the most efficient (because the constructor chose the best parameters and algorithms)
  - The most portable for performance.
- ### Disadvantages:
  - Little control over the mode actually used (often accessible via environment variables).
  - Risk of deadlocks depending on the mode used.
  - Behavior can vary according to the architecture and problem size.


---
<p class="chapter">Communication Modes</p>

### Interface of `MPI_Bsend`  (Exemple 14)

```C

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
    int typesize, overhead, size;
    int nb_msg=1, nb_elt=1;
    int rank, num_proc, value, result, orank, tag=110;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Type_size(MPI_INT,&typesize);

    overhead = (1+(MPI_BSEND_OVERHEAD*1.))/typesize;
    int *buf = malloc(nb_msg*(nb_elt*overhead));

    size = typesize*nb_msg*(nb_elt+overhead);

    MPI_Buffer_attach(buf,size);

    // only 2 procs
    orank = (rank+1)%2;
    value = rank+1000;

    MPI_Bsend(&value,1,MPI_INT,orank,tag,MPI_COMM_WORLD);

    MPI_Recv(&result,1,MPI_INT,orank,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

    printf("I process %d , receveid %d ; from process %d\n",rank,result,orank);
    MPI_Buffer_detach(buf,&size);
    MPI_Finalize();
    return 0;
}
```


---
<p class="chapter">Communication Modes</p>


<h3 class="left2">Standard sends</h3>

A standard send is made by calling the `MPI_Send` subroutine. In most implementations, the mode is buffered (eager) for small messages but is synchronous for larger messages.

<h3 class="left">The Interface:</h3>
&nbsp;


```C
int MPI_Send(void *buf,  int count,
              MPI_Datatype datatype,
              int dest,
              int tag,
              MPI_Comm comm
);
```


---
<p class="chapter">Communication Modes</p>

### The eager protocol
<div class="box" id="part-1">
The  eager  protocol  is  often  used for  standard  sends  of  small-sizemessages.

It  can  also  be  used for  sends  with `MPI_Bsend` for small  messages (implementation-dependent)  and  by  bypassing  the user buffer on the sender side.

In this approach, the buffer is on the receiver side.

The return receipt is optional.

 </div>
<div class="box" id="part-2">
<img src="../figures/eager_protocol.png" width="100%" style="justify-content: center"></img>
</div>



---
<p class="chapter">Communication Modes</p>

<h3 class="left">Non blocking:</h3>

  - The overlap of communications by computations is a method which allows executing communications operations in the background while the program continues to operate.

  - #### Exemple :
  On Ada, the latency of a communication internode is 1.5μs, or 4000 processor cycles.
    It is thus possible, if the hardware and software architecture allows it, to hide all or part of communications costs.

  - The computation-communication overlap can be seen as an additional level of parallelism.

  - This approach is used in MPI by using nonblocking subroutines (i.e. `MPI_Isend`, `MPI_Irecv` and `MPI_Wait`).
  - A non blocking call returns very quickly but it does not authorize the immediate re-useof the memory space which was used in the communication.

  - It is necessary to make sure that the communication is fully completed (with `MPI_Wait`, for example) before using it again.



---
<p class="chapter">Communication Modes</p>

### Non blocking

<img src="../figures/send_overlap.png" width="100%" style="justify-content: center"></img>


---
<p class="chapter">Communication Modes</p>

### Non blocking

- ### Advantages:
  - Possibility of hiding all or part of communications costs (if the architecture allows it).
  - No risk of deadlock.

- ### Disadvantages:
  - Greater additional costs (several calls for one single send or receive, request management).
  - Higher complexity and more complicated maintenance.
  - Less efficient on some machines (for example with transfer starting only at the `MPI_Wait` call).
  - Risk of performance loss on the computational kernels (for example, differentiated management between the area near the border of a domain and the interior area,resulting in less efficient use of memory caches).
  - Limited to point-to-point communications (it is extended to collective communications in MPI 3.0).


---
<p class="chapter">Communication Modes</p>

### Interfaces MPI_Isend MPI_Issend and MPI_Ibsend for non-blocking send

```C
int MPI_Isend( void *buf, int cnt, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *req);

int MPI_Issend( void *buf, int cnt, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *req);

int MPI_Ibsend( void *buf, int cnt, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *req);
```

- `buf` [in] initial address of send buffer (choice)
- `count` [in] number of elements in send buffer (integer)
- `datatype` [in] datatype of each send buffer element (handle)
- `dest` [in] rank of destination (integer)
- `tag` [in] message tag (integer)
- `comm` [in] communicator (handle)
- `request` [out] communication request (handle)

And for the non-blocking reception

```C
int MPI_Irecv( void *buf, int cnt, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request *req);
```


---
<p class="chapter">Communication Modes</p>

### Interfaces MPI_Wait and MPI_Test (non-bloocking version)

```C
int MPI_Wait( MPI_Request *request, MPI_Status *status);

int MPI_Test( MPI_Request *request, int *flag, MPI_Status *status);
```
- `request` [in] request (handle)
- `status` [out] status object (Status). May be `MPI_STATUS_IGNORE`.
- `flag` [out] true if operation completed (logical)



---
<p class="chapter">Communication Modes</p>

### Interfaces MPI_Waitall and MPI_Testall (non-bloocking version)

```C
int MPI_Waitall( int count, MPI_Request array_of_requests[], MPI_Status array_of_statuses[]);

int MPI_Testall( int count, MPI_Request array_of_requests[], int *flag, MPI_Status array_of_statuses[]);
```

- `count` [in] list length (integer)
- `array_of_requests` [in] array of request handles (array of handles)
- `array_of_statuses` [out] array of status objects (array of Statuses). May be `MPI_STATUSES_IGNORE`.
- `flag` [out] true if operation completed (logical)


---
<p class="chapter">Communication Modes</p>

### Interface MPI_Waitany

```C
int MPI_Waitany( int count, MPI_Request array_of_requests[], int *index, MPI_Status *status);
```

- `count` [in] list length (integer)
- `array_of_requests` [in/out] array of requests (array of handles)
- `index`  [out] index of handle for operation that completed (integer).
- `status`  [out] status object (Status). May be `MPI_STATUS_IGNORE`.


---
<p class="chapter">Communication Modes</p>

### Interfaces MPI_Waitsome and MPI_Testsome (non-bloocking version)

```C
int MPI_Waitsome(int incount,MPI_Request array_of_requests[], int *outcount,
    int array_of_indices[], MPI_Status array_of_statuses[]);
```

- `incount`  [in] length of array_of_requests (integer)
- `array_of_requests`  [in] array of requests (array of handles)
- `outcount` [out] number of completed requests (integer)
- `array_of_indices` [out] array of indices of operations that completed (array of integers)
- `array_of_statuses` [out] array of status objects for operations that completed (array of Status). May be `MPI_STATUSES_IGNORE`.


---
<p class="chapter">Communication Modes</p>

<h3 class="left2"> Request management</h3>

- After a call to a blocking wait function (`MPI_Wait`, `MPI_Waitall`,...), the request argument is set to `MPI_REQUEST_NULL`.
- The same for a non-blocking wait when the flag is set to true.
- A wait call with a `MPI_REQUEST_NULL` request does nothing.


---
<p class="chapter">Communication Modes</p>

### Exemple : non-blocking communications

<img src="../figures/comm_voisins.png" width="45%"></img>

---
<p class="chapter">Communication Modes</p>

### Exemple : non-blocking communications

```C
void start_communication(*u){
    // Send to the North and receive from the South
    MPI_Irecv( u, 1, rowtype, neighbor(S), &4tag, comm2d, request[0]);
    MPI_Isend( u, 1, rowtype, neighbor(N), &6tag, comm2d, request[1]);

    //Send to the South and receive from the North;
    MPI_Irecv( u, 1,rowtype, neighbor(N), &10tag, comm2d, request[2]);
    MPI_Isend(  u, 1,rowtype,neighbor(S), &12tag, comm2d, request[3]);

    // Send to the West and receive from the East;
    MPI_Irecv( u, 1, columntype, neighbor(E), &16tag, comm2d, request[4]);
    MPI_Isend( u, 1, columntype, neighbor(W), &18tag, comm2d, request[5]);

    // Send to the East and receive from the West
    MPI_Irecv( u, 1, columntype, neighbor(W), &22tag, comm2d, request[6]);
    MPI_Isend( u, 1, columntype, neighbor(E), &24tag, comm2d, request[7]);
}

void end_communication(*u){
     MPI_Waitall(2*NB_NEIGHBORS,request,tab_status);
}
```

---
<p class="chapter">Communication Modes</p>

### Exemple : non-blocking communications

```C
do while(! convergence && it < it_max){
    it ++;
    u(sx:ex,sy:ey) = u_new(sx:ex,sy:ey)
    // Exchange value on the interfaces
    start_communication( u )
    // Compute u
    calcul( u, u_new, sx+1, ex-1, sy+1, ey-1);
    end_communication( u );
    // North
    calcul( u, u_new, sx, sx, sy, ey);
    // South
    calcul( u, u_new, ex, ex, sy, ey);
    // West
    calcul( u, u_new, sx, ex, sy, sy);
    //East
    calcul( u, u_new, sx, ex, ey, ey);
    //Compute global error
    diffnorm =  global_error (u, u_new);
    convergence = ( diffnorm < eps );
}
```

---
<p class="chapter">Communication Modes</p>

### Exercice 3.7 : non-blocking communications

- For this exercise use `MPI_Isend` to do a non-blocking send of information from the root process to a destination process.
- The destination process is set as a variable in the code and must be less than the number of processes started (4).

---
<p class="chapter">Communication Modes</p>

### Example 14.5  : Barrier vs Waitall

```C
    // Get my rank
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if(my_rank == 0)
    {
        // The "master" MPI process sends the message.
        int buffer[2] = {12345, 67890};
        MPI_Request requests[3];
        printf("MPI process %d sends the values %d & %d.\n", my_rank, buffer[0], buffer[1]);
        MPI_Isend(&buffer[0], 1, MPI_INT, 1, 0, MPI_COMM_WORLD, &requests[0]);
        MPI_Isend(&buffer[1], 1, MPI_INT, 2, 0, MPI_COMM_WORLD, &requests[1]);

        // Wait for both routines to complete
	//MPI_Barrier(MPI_COMM_WORLD);
        MPI_Waitall(2, requests, MPI_STATUSES_IGNORE);
        printf("Process %d: both messages have been sent.\n", my_rank);
    }
    else
    {
        // The "slave" MPI processes receive the message.
        int received;
        MPI_Recv(&received, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        printf("Process %d received value %d.\n", my_rank, received);
    }

```

---
<p class="chapter">Communication Modes</p>

### One-Sided Communications

<div class="box" id="part-1">

One-sided communications (RemoteMemory Access or RMA) consists of accessing the memory of a distant process in read or write without the distant process having to manage this access explicitly. The target process does not intervene during the transfer.
 </div>
<div class="box" id="part-2">
<img src="../figures/put.png" width="100%" style="justify-content: center"></img>
</div>


---
<p class="chapter">Communication Modes</p>

### One-Sided Communications

- #### General approach
    - Creation of a memory window with `MPI_Win_create` to authorize RMA transfers in this zone.
    - Remote access in read or write by calling `MPI_Put`, `MPI_Get` and `MPI_Accumulate`.
    - Free the memory window with `MPI_Win_free`.

- #### Synchronization methods
     In order to ensure the correct functioning of the application, it is necessary to execute some synchronizations. Three methods are available :
    - Active target communication with global synchronization (`MPI_Win_fence`).
    - Active target communication with synchronization by pair (`MPI_Win_start` and `MPI_Win_complete` for the origin process; `MPI_Win_post` and `MPI_Win_wait` for the target process).
    - Passive target communication without target intervention (`MPI_Win_lock` and `MPI_Win_unlock`).


---
<p class="chapter">Communication Modes</p>

### Exemple 15 : One-Sided Communications

```C
int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    // Get my rank
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // Create the window
    const int ARRAY_SIZE = 2;
    int window_buffer[ARRAY_SIZE];
    MPI_Win window;
    if(my_rank == 1)
    {
        window_buffer[1] = 67890;
    }
    MPI_Win_create(window_buffer, ARRAY_SIZE * sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &window);
    MPI_Win_fence(0, window);
}
    int remote_value;
    if(my_rank == 0)
    {
        // Fetch the second integer in MPI process 1 window
        MPI_Get(&remote_value, 1, MPI_INT, 1, 1, 1, MPI_INT, window);

        // Push my value into the first integer in MPI process 1 window
        int my_value = 12345;
        MPI_Put(&my_value, 1, MPI_INT, 1, 0, 1, MPI_INT, window);
    }

```

---
<p class="chapter">Communication Modes</p>

### Exemple 15 : One-Sided Communications

```C
    // Wait for the MPI_Get and MPI_Put issued to complete before going any further
    MPI_Win_fence(0, window);

    if(my_rank == 0)
    {
        printf("[MPI process 0] Value fetched from MPI process 1 window_buffer[1]: %d.\n", remote_value);
    }
    else
    {
        printf("[MPI process 1] Value put in my window_buffer[0] by MPI process 0: %d.\n", window_buffer[0]);
    }

    // Destroy the window
    MPI_Win_free(&window);

    MPI_Finalize();

    return 0;
}
```

```bash
mpiexec --oversubscribe --bind-to none -n 2 ./exemple_15
[MPI process 0] Value fetched from MPI process 1 window_buffer[1]: 67890.
[MPI process 1] Value put in my window_buffer[0] by MPI process 0: 12345.
```


---
<p class="chapter">Communication Modes</p>

### One-Sided Communications

- ### Advantages:
  - Certain algorithms can be written more easily.
  - More efficient than point-to-point communications on certain machines (use of specialized hardware such as a DMA engine, coprocessor, specialized memory,...).
  - The implementation can group together several operations.
- ### Disadvantages:
  - Synchronization management is tricky.
  - Complexity and high risk of error.
  - For passive target synchronizations, it is mandatory to allocate the memory with `MPI_Alloc_mem`.
  - Less efficient than point-to-point communications on certain machines.
