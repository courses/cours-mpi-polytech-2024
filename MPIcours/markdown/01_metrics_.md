# Performance Metrics

---

<p class="chapter">Metrics</p>

## Speedup

- Computation time is an immediate and natural metric to evaluate
    the performance of a parallel program.

- Thus, we define the function called **speedup** as follows:
  $$S(p)=\frac{T(1)}{T(p)}$$ with $T(p)$ = computation time
  of an execution on $p\in\mathbb{N}^+$ cores.

---

<p class="chapter">Metrics</p>

## Theoretical speedup vs measured speedup

- Strive to approach $S(p)=p$ as closely as possible.
- If $S(p)<1$, parallelization slows down. The algorithm needs to be
  reviewed and corrected.

- If $S(p)>p$, it's abnormal and needs understanding (e.g. the
  original algorithm has been modified and converges faster).

<img src="../figures/speedupd.jpeg" width="40%" style="justify-content: center"></img>

---

<p class="chapter">Metrics</p>

## Speedup and Efficiency

We also define the function of **Efficiency** as:
$$E(p)=\frac{S(p)}{p}$$
the rate of resource utilization, aiming closest to 1 possible!.

---

<p class="chapter">Metrics</p>

## Scalability

- **Speedup** and **efficiency** functions are intuitive but it's not
  always possible to finish the sequential execution (memory shortage,
  too long, etc.) and thus to calculate them.

- Therefore, we use metrics of **scalability** to evaluate the
system's capacity to <span class="myred" >handle larger problems as
the resource quantity increases.</span>

---

<p class="chapter">Metrics</p>

### Scalability (Size Up)

- Verify that **computation time remains relatively constant as the
problem size and machine power** :  number of cores, memory capacity, network.

<!-- ![speedup](../figures/scaling.png "scalability") -->
<img src="../figures/scaling.png" width="80%" style="justify-content: center"></img>

---

<p class="chapter">Metrics</p>

### Scalability (Extendibility)

Ideally, $T(p) = T(1)/p$ ; then $$\log(T(p)) = \log(T(1)) - log(p)$$

<!-- ![extensibility](../figures/extensibility.png) -->
<img src="../figures/extensibility.png" width="80%" style="justify-content: center"></img>


where $n$ is the system size.

---

<p class="chapter">Metrics</p>

## Some Remarks

- You'll never get perfect results!
- Therefore, you need to *analyze to understand* what's happening.
- <span class="myred"> Sources of slowdown </span>:
- Significant <span class="myred">sequential fraction</span>,
  - Poor cache memory management,
  - Overhead of parallelism management operations,
  - Load imbalance,
  - Expensive I/O operations (access to network disks),
  - and many more!

---

<p class="chapter">Metrics</p>

## Amdahl's Law

<!-- ![extensibility](../figures/seq-par.png "sequential and parallel parts") -->
<img src="../figures/seq-par.png" width="80%" style="justify-content: center"></img>

What is the impact on performance of the <span class="myred">sequential fraction</span>?

---

<p class="chapter">Metrics</p>

## Amdahl's Law

- Let $T(1) = t_{seq} + t_{par}$ with:
  - $t_{seq}$ : the time required to execute the sequential part of the program
  - $t_{par}$ : the time required to execute its parallelizable part.
- If we assume a perfect parallelization:
    $$T(p) = t_{seq} + \frac{t_{par}}{p}$$

---

<p class="chapter">Metrics</p>

## Amdahl's Law

- Let $f$ be the sequential fraction of the program:
    $$f = \frac{t_{seq}}{t_{seq}+t_{par}}$$
- We get (simplified...):
    $$S(p) = \frac{T(1)}{T(p)} = \frac{t_{seq}+t_{par}}{t_{seq}+\frac{t_{par}}{p}} \left( \frac{\frac{1}{t_{seq}+t_{par}}}{\frac{1}{t_{seq}+t_{par}}} \right) = \frac{1}{f+\frac{1-f}{p}}$$

---

<p class="chapter">Metrics</p>

## Amdahl's Law

- Speedup is bounded by $\frac{1}{f}$.
- *A small sequential fraction implies significant limitation when $p$
  is large*.

<!-- ![speedup](../figures/amdhal_limit.png "speedup") -->
<img src="../figures/amdhal_limit.png" width="80%" style="justify-content: center"></img>
