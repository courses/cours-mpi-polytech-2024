# Point-point communications

## General Concepts

A point-to-point communication occurs between two processes : the
<span class="myred"> **sender** </span> process and the  <span
class="myred">**receiver**</span> process.

<img src="../figures/pt_pt_gen.png" width="40%" style="justify-content: center"></img>

---

<p class="chapter">Point-point comm.</p>

### General Concepts

- The <span class="myred">**sender**</span> and the <span
  class="myred">**receiver**</span> are identified by their ranks in
  the communicator.

- The object communicated from one process to another is called <span
  class="myred">**message**</span>.

- A message is defined by its envelope, which is composed of :
  - the rank of the sender process
  - the rank of the receiver process
  - the message tag
  - the communicator in which the transfer occurs
  - The exchanged data has a datatype (integer, real, etc, or individual
  derived datatypes).

- There are several transfer modes, which use different protocols.

---
<p class="chapter">Point-point comm.</p>

## Blocking Send : MPI_Send

```c
int MPI_Send(
  void *buf,
  int count,
  MPI_Datatype datatype,
  int dest,
  int tag,
  MPI_Comm comm
);
```

Sending, from the address `buf`, a message of `count` elements of type
`datatype`, tagged `tag`, to the process of rank `dest` in the
communicator `comm`.

<span class="myred" style="position:left">**REMARK**</span> :

This call is blocking: the execution remains blocked until the
message can be re-written without risk of overwriting the value to be
sent.

In other words, the execution is blocked as long as the message has
not been received.

---
<p class="chapter">Point-point comm.</p>

##  Blocking Receive: MPI_Recv

```c
int MPI_Recv(
  void *buf,
  int count,
  MPI_Datatype datatype,
  int source,
  int tag,
  MPI_Comm comm,
  MPI_Status *status
);
```

Receiving, at the address `buf`, a message of `count` elements of type
`datatype`, tagged `tag`, from the process of rank `source` in the
communicator `comm`.

<span class="myred">**REMARK**</span> :

`status` stores the state of a receive operation : `source`, `tag`,
`code`, ... .

An `MPI_Recv` can only be associated to an `MPI_Send` if these two
calls have the same envelope (`source`, `dest`, `tag`, `comm`).This
call is blocking : the execution remains blocked until the message
content corresponds to the received message.

---
<p class="chapter">Point-point comm.</p>

## Example 2 : MPI_Send - MPI_Recv

```c []
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
  int rank, value;
  int tag = 100;
  MPI_Status status;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if(rank==2){
    value = 1000;
    MPI_Send(&value,1,MPI_INT,5,tag,MPI_COMM_WORLD);
  }
  else if(rank==5){
    MPI_Recv(&value,1,MPI_INT,2,tag,MPI_COMM_WORLD,&status);
    printf("I process 5, I received %d from process 2\n",value);
  }
  MPI_Finalize();
  return 0;
}
```

```bash
> mpirun -n 6 ./exemple_02
I process 5, I received 1000 from process 2
```

---
<p class="chapter">Point-point comm.</p>

## C DataTypes

| MPI types         | C types     |
| :------------- | :------------- |
|MPI_CHAR |signed char|
|MPI_SHORT | signed short |
|MPI_INT   | signed int |
|MPI_LONG|signed long int |
|MPI_UNSIGNED_CHAR | unsigned char|
|MPI_UNSIGNED_SHORT|unsigned short |
|MPI_UNSIGNED| unsigned int|
|MPI_UNSIGNED_LONG|unsigned long int|
|MPI_FLOAT|float |
|MPI_DOUBLE|double|
|MPI_LONG_DOUBLE|long double|

---
<p class="chapter">Point-point comm.</p>

## Other possibilities

- When receiving a message, the rank of the sender process and the tag
  can be replaced by «jokers» : `MPI_ANY_SOURCE` and `MPI_ANY_TAG`,
  respectively.

- A communication involving the dummy process of rank `MPI_PROC_NULL`
  has no effect.

- `MPI_STATUS_IGNORE` is a predefined constant, which can be used
  instead of the status variable.

- It is possible to send more complex data structures by creating
  derived datatypes.

- There are other operations, which carry out both send and receive
  operations simultaneously : `MPI_Sendrecv` and
  `MPI_Sendrecv_replace`.

---
<p class="chapter">Point-point comm.</p>

## Simultaneous send and receive : MPI_Sendrecv

```c
int MPI_Sendrecv(
  void *sendbuf,
  int sendcount,
  MPI_Datatype sendtype,
  int dest,
  int sendtag,
  void *recvbuf,
  int recvcount,
  MPI_Datatype recvtype,
  int source,
  int recvtag,
  MPI_Comm comm,
  MPI_Status *status
);

```

- Sending, from the address `sendbuf`, a message of `sendcount`
  elements of type `sendtype`, tagged `sendtag`, to the process `dest`
  in the communicator `comm` ;

- Receiving, at the address `recvbuf`, a message of `recvcount`
  elements of type `recvtype`, tagged `recvtag`, from the process
  source in the communicator `comm`.

**Remark** : Here, the receiving zone `recvbuf` must be different
  from the sending zone `sendbuf`.

---
<p class="chapter">Point-point comm.</p>

## Simultaneous send and receive : MPI_Sendrecv_replace

```c
int MPI_Sendrecv_replace(
  void *buf,
  int count,
  MPI_Datatype datatype,
  int dest,
  int sendtag,
  int source,
  int recvtag,
  MPI_Comm comm,
  MPI_Status *status
);

```

- Sending, from the address `buf`, a message of `count` elements of type `datatype`, tagged `sendtag`, to the process `dest` in the communicator `comm` ;
- Receiving, at the address `buf`, the same message of  `datatype`, tagged `recvtag`, from the process source in the communicator `comm`.

---
<p class="chapter">Point-point comm.</p>

## Example 3 : MPI_Sendrecv

```c []
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
  int rank, num_proc, send_val ,recv_val;
  int tag = 100;
  MPI_Status status;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  recv_val = 0;
  send_val = rank+1000;

  // We define the process we'll communicate with (we suppose that we have exactly 2 processes)
  num_proc = (rank+1)%2;

  MPI_Sendrecv(&send_val,1,MPI_INT,num_proc,tag,&recv_val,1,MPI_INT,num_proc,tag,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

  printf("Coucou, I process %d, I received %d from preocess %d \n", rank,recv_val,num_proc);

  MPI_Finalize();
  return 0;
  }
```

---
<p class="chapter">Point-point comm.</p>

## Example 3 : MPI_Sendrecv

<img src="../figures/exemple_03.png" width="40%" style="justify-content: center"></img>

```bash
> mpirun -n 2 ./exemple_03
Coucou, I process 1, I received 1000 from preocess 0
Coucou, I process 0, I received 1001 from preocess 1
```

---
<p class="chapter">Point-point comm.</p>

## Example 3: MPI_Sendrecv

### <span class="myred">**Be careful !**</span>
If we replace the `MPI_Sendrecv` in the example above by `MPI_Send` followed by `MPI_Recv`, the code will deadlock.
Indeed, each of the two processes will wait for a receipt confirmation,
which will never come because the two sending operations would stay suspended :

```c
send_val = rank+1000
MPI_Send(&(send_val),1,MPI_INTEGER,num_proc,tag,MPI_COMM_WORLD)
MPI_Recv(&recv_val),1,MPI_INTEGER,num_proc,tag,MPI_COMM_WORLD,status_msg)

```

---
<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong

### Point to point communications :Ping-Pong between two processes

<span style="position:left;"> This exercise is composed of 3 steps :</span>

- <span class="myred">**Ping**</span>: complete `ping_pong_1.c` in such a way that the process 0 send a message containing 1000 random reals to process 1.
- <span class="myred">**Ping-Pong**</span>: complete `ping_pong_2.c` in such a way that the process 1 sends back the message to the process 0, and measure the communication    duration with the `MPI_Wtime` function.
- <span class="myred">**Ping-Pong match**</span>: complete `ping_pong_3.c` in such a way that processes 0 and 1 perform 9 Ping-Pong, while varying the message size, and measure the communication duration each time. The corresponding bandwidths will be printed.

---
<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong

### <span class="myred">**Remarks**</span>

- The generation of random numbers uniformly distributed in the range [0,1[ is made by calling the `rand()` random_number function :

```c
double variable;
variable = rand();
```

- The time duration measurements can be done like this :

```c
time_begin = MPI_WTIME();
...................
time_end = MPI_WTIME();
printf ("... in %f8.6 secondes.\n",time_end-time_begin);
```

---
<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong Solution with MPI_Sendrecv

```c
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
  int rang, iter, orang, nb_valeurs=1000, tag=99;
  double valeurs[nb_valeurs], buffer[nb_valeurs];
  MPI_Status statut;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rang);

  if (rang == 0) {for (iter = 0; iter<nb_valeurs; iter++){valeurs[iter] = 1.;buffer[iter] = 0;}}

  printf("t0, processus %d, j'ai value=%f e buffer=%f\n",rang,valeurs[0],buffer[0]);
  orang = (rang+1)%2;

  MPI_Sendrecv(valeurs,nb_valeurs,MPI_DOUBLE,orang,tag,
               buffer,nb_valeurs,MPI_DOUBLE,orang,tag,MPI_COMM_WORLD,&statut);

  printf("t1, processus %d, j'ai value=%f e buffer=%f\n",rang,valeurs[0],buffer[0]);
  if (rang == 0) {for (iter = 0; iter<nb_valeurs; iter++){  valeurs[iter] = 0;}}

  MPI_Sendrecv(buffer,nb_valeurs,MPI_DOUBLE,orang,tag,
               valeurs,nb_valeurs,MPI_DOUBLE,orang,tag,MPI_COMM_WORLD,&statut);

  printf("t2, processus %d, j'ai value=%f e buffer=%f\n",rang,valeurs[0],buffer[0]);
  MPI_Finalize();
  return 0;
}
```

---

<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong Solution with MPI_Sendrecv

```bash
>mpiexec -n 2 ping_pong_other
t0, processus 0, j'ai value=1.000000 et buffer=0.000000
t0, processus 1, j'ai value=0.000000 et buffer=0.000000
t1, processus 0, j'ai value=1.000000 et buffer=0.000000
t1, processus 1, j'ai value=0.000000 et buffer=1.000000
t2, processus 0, j'ai value=1.000000 et buffer=0.000000
t2, processus 1, j'ai value=0.000000 et buffer=1.000000
```

---

<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong Solution with MPI_Sendrecv

```c
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
    int rang,iter,orang,nb_valeurs=1000, tag=99;
    double valeurs[nb_valeurs];
    MPI_Status statut;
    double temps_debut,temps_fin;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rang);
    if(rang==0)for(iter=0;iter<nb_valeurs; iter++){valeurs[iter]=1.;}
    printf("t0, processus %d, j'ai value=%f \n",rang,valeurs[0]);
    orang = (rang+1)%2;  // orang est le rang de l'autre processus

    MPI_Sendrecv_replace(valeurs,nb_valeurs,MPI_DOUBLE,orang,tag,
			                   orang,tag,MPI_COMM_WORLD,&statut);
    printf("t1, processus %d, j'ai value=%f \n",rang,valeurs[0]);

    MPI_Sendrecv_replace(valeurs,nb_valeurs,MPI_DOUBLE,orang,tag,
		                     orang,tag,MPI_COMM_WORLD,&statut);
    printf("t2, processus %d, j'ai value=%f \n",rang,valeurs[0]);

    MPI_Finalize();
    return 0;
}
```

---

<p class="chapter">Point-point comm.</p>

## Exercise 2 : Ping-Pong Solution with MPI_Sendrecv

```bash
>mpiexec -n 2 ping_pong_other_2
t0, processus 0, j'ai value=1.000000
t0, processus 1, j'ai value=0.000000
t1, processus 0, j'ai value=0.000000
t1, processus 1, j'ai value=1.000000
t2, processus 0, j'ai value=1.000000
t2, processus 1, j'ai value=0.000000
```
