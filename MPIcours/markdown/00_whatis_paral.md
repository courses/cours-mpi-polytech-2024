### Sequential progamming model

- The program is executed by one and only one process.
- All the variables and constants of the program are allocated in the memory of the process.
- A process is executed on a physical processor of the machine

<img src="../figures/seq_prog.png" width="50%" style="justify-content: center"></img>

---

<p class="chapter">Introduction</p>

### Parallel progamming model

- All the program variables are private and reside in the local memory of each process.
- Each process has the possibility of executing different parts of a program.
- A variable is exchanged between two or several processes via a programmed call
to specific subroutines.

<img src="../figures/paral_prog.png" width="40%" style="justify-content: center"></img>


---

<p class="chapter">Introduction</p>

### Why to write parallel programs ?

There are many motivations:
- To reduce the restitution time (**<span class="myred">speed</span>**)
- To perform larger simulation (**<span class="myred">size</span>**)
- To exploit parallelism of modern processor architectures (multi-cores, multi-threading)

---

<p class="chapter">Introduction</p>

### A bit of jargon before diving deeper...
- **Supercomputing · High Performance Computing (HPC):** Using the most powerful computers to solve large-scale problems.
- **Node:** A computer equipped with at least one or more processors, memory, and a network interface. Several nodes are typically networked together to form a computing cluster (cluster, supercomputer).
- **Socket · CPU = Processor · Core:** A socket is a physical connector on the motherboard designed to receive a processor (hardware component). A processor can itself contain multiple cores/processors (execution units). It can be noted here that the term "processor" can vary in meaning from one person to another.

---

<p class="chapter">Introduction</p>

### A bit of jargon before diving deeper...
<span class="left3" style="font-weight:bold">Synthesis</span>

A computing cluster is a collection of multiple nodes.
Each node has one or more sockets, each capable of accommodating a specific processor.
A processor itself is composed of one or more cores.


---

<p class="chapter">Introduction</p>

## Simplified 2-nodes structure

<img src="../figures/simple_node.png" width="40%" style="justify-content: center"></img>

---

<p class="chapter">Introduction</p>

## Realistic 2-nodes configuration

<img src="../figures/cluster_2_nodes.png" width="80%" style="justify-content: center"></img>

---

<p class="chapter">Introduction</p>

## Hierarchical structure in the cluster

<img src="../figures/cluster.png" width="60%" style="justify-content: center"></img>


---

<p class="chapter">Introduction</p>

## Types of Parallelization

### Shared Memory Parallelization

- **Description:** In shared memory parallelization, multiple processors access a common pool of memory.
- **Communication:** Processors communicate by reading and writing to shared memory locations.
- **Advantages:**
  - Simplified programming model.
  - Efficient for tasks that require frequent data sharing among processors.
- **Examples:** <span class="myred">OpenMP</span>, Pthreads.

### Distributed Memory Parallelization

- **Description:** In distributed memory parallelization, each processor has its own memory, and communication occurs explicitly between processors.
- **Communication:** Processors exchange data through message passing.
- **Advantages:**
  - Scalability for large-scale systems.
  - Better suited for tasks with minimal data sharing.
- **Examples:** <span class="myred">MPI (Message Passing Interface)</span>, Hadoop.

---

<p class="chapter">Introduction</p>

## Types of Parallelization

OpenMP uses a shared memory paradigm, while MPI uses a distributed memory paradigm.

<img src="../figures/openmp_vs_mpi.png" width="100%" style="justify-content: center"></img>


## Hybrid Models

-   It is possible to **mix the two approaches** by interconnecting shared-memory machines.
