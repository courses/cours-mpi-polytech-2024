<h1 style="display:none">Title</h1>

# <span class="myred">Calcul parallèle et distribuè, </span> grilles <span class="myred">et calculs</span>
##### Ronan Bocquillon  &  <span style="text-decoration:underline">Marco Mancini</span>

---

# Parallelization Course Presentation

- ### Structure
  - 8 hours of CM, 2 hours of TD, and 14 hours TP.
  - 2 hours for examination.

- ### Program
  - Introduction to Parallel Computing.
  - Performance Evaluation of Programs.
  - Introduction to OpenMP.
  - MPI (Message Passing Interface).
  - Hybrid Programming.

---

## Basic concepts

- Software typically performs its calculations <span class="myred">sequentially</span>.
  - A program is a series of instructions (steps).
  - The instructions are executed one after the other, on a single processor.

- Parallel computing involves solving problems by <span class="myred">simultaneously exploiting multiple computing resources</span>.
  - A program consists of several independent <span class="myred">tasks</span>.
  - A task is itself a sequence of instructions.
  - The instructions of each task are executed simultaneously on <span class="myred">multiple</span>
  processors (depending on the available resources).
  - A control and coordination mechanism is implemented to address the initial problem.
