# MPI

## <span class="myred">  (Message Passing Interface) </span>

<h1 style="display:none">Environment</h1>

## The concept

If a message is sent to a process, the process must receive it

<img src="../figures/send_recv.png" width="40%" style="justify-content: center"></img>

---

<p class="chapter">Environment</p>

## Environment

- The exchanged messages are interpreted and managed by an environment comparable to telephony, e-mail, postal mail, etc.
- The message is sent to a specified address.
- The receiving process must be able to classify and interpret the messages which are sent to it.
- The environment in question is MPI (Message Passing Interface).

<span class="myburnt fragment fade-up "> An MPI application is a group of autonomous processes, each executing its own code and communicating via calls to MPI library subroutines.</span>

---

<p class="chapter">Environment</p>

## Message content

- A message consists of data chunks passing from the sending process to the receiving process/processes.
- In addition to the data (scalar variables, arrays, etc.) to be sent, a message must contain the following information :

  - The identifier of the sending process
  - The datatype
  - The length
  - The identifier of the receiving process

<img width="50%"  src="../figures/message.png" style="justify-content:center"></img>

---

<p class="chapter">Environment</p>

## The communicators

A communicator defines a <span class="myred"> **group of processes**</span> that have the ability to communicate with one another. In this group of processes, each is assigned a unique <span class="myred"> **rank**</span>, and they explicitly communicate with one another by their ranks.

<img src="../figures/communicator.png" width="40%" style="justify-content: center"></img>

The default communicator is `MPI_COMM_WORLD` which include all the active processes

---

<p class="chapter">Environment</p>

## Domain decomposition

A schema that we often see with MPI is domain decomposition :

Each process controls a part of the global domain and mainly
communicates with its neighbouring processes.

<img src="../figures/subdomain_intro.png" width="40%"
style="justify-content: center"></img>

---

<p class="chapter">Environment</p>

## Using MPI

- Every program unit calling MPI subroutines has to include a header
  file (`mpi.h`).
- The function `MPI_Init()` initializes the MPI environment.
- The function `MPI_Finalize()` disable it.

```C
int MPI_Init(int*argc, char***argv);

int MPI_Finalize(void);
```

---

<p class="chapter">Environment</p>

## Rank and size

At any moment, we have access to the number of processes managed by a
given communicator by calling the <code>MPI_Comm_size</code> function

```c
int MPI_Comm_size(
  MPI_Comm comm,
  int *size
);
```

</br></br>
Similarly, the function <code>MPI_Comm_rank</code> allows us to obtain
the rank of an active process (i.e. its instance number, between 0 and
<code>MPI_Comm_size()– 1</code>).

```c
int MPI_Comm_rank(
  MPI_Comm comm,
  int *rank
);
```

---

<p class="chapter">Environment</p>

## Aborting programs

Sometimes, a program encounters some issue during its execution and
has to stop prematurely. For example, we want the execution to stop if
one of the processes cannot allocate the memory needed for its
calculation.

In this case, we call the <code>MPI_Abort()</code>.

```c
int MPI_Abort(
  MPI_Comm comm,
  int errorcode
);
```

- `comm` :  the communicator of which all processes will be stopped
- `errorcode` :  the error to return to the `UNIX` environment.

---

<p class="chapter">Environment</p>

## Compilation and execution of an MPI code

- To compile an MPI code, we use a <span class="myred" >compiler
wrapper</span>, which makes the link with the chosen MPI library.

- This wrapper is different depending on the programming language, the
compiler and the MPI library.

- For example : `mpif90`, `mpifort`, `mpicc`.

```sh
> mpicc <options> -c source.c -o  my_executable_file
```

- To execute an MPI code, we use an MPI launcher, which runs the
execution on a given number of processes.

- The `mpiexec` launcher is defined by the MPI standard.

```sh
> mpiexec -n <number of processes> my_executable_file
```

- There are also non-standard launchers, such as `mpirun`.

- **PS** To simulate a bigger number of processus than the physical
ones, use :

```sh
mpiexec --oversubscribe ....
```

---

<p class="chapter">Environment</p>

## Example 1

```c []
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
  int rank, nb_procs;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nb_procs);

  printf("Coucou, je suis le processus  %d sur %d processes\n", rank,nb_procs);

  MPI_Finalize();
}
```

```bash
> mpirun -n 4 ./exemple_01
Coucou, je suis le processus  3 sur 4 processes
Coucou, je suis le processus  1 sur 4 processes
Coucou, je suis le processus  2 sur 4 processes
Coucou, je suis le processus  0 sur 4 processes
```

---

<p class="chapter">Environment</p>

## Exercise 1 : MPI environment

- Write an MPI program in such a way that each process prints a
  message, which indicates whether its rank is odd or even.

  For example:

```sh
> mpiexec -n 4 ./even_odd
  I am process 0, my rank is even
  I am process 2, my rank is even
  I am process 3, my rank is odd
  I am process 1, my rank is even
```

- To test whether the rank is odd or even, use the modulo function `a%b`.

- To compile and execute your program, use the command `make`.

- To be recognized by the `Makefile`, the file must me named
  `even_odd.c`.
