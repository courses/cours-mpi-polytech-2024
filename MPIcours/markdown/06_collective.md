## Collective Communications


---
<P class="chapter">Collective Communications</p>

## Collective Communications

<h3 class="left">General Concepts</h3>

- Collective communications allow to make a series of point-to-point communications in a single call.
- A collective communication always concerns all the processes of the indicated communicator.
- For each process, the call ends when its participation in the collective call is completed, in the sense of point-to-point communications (therefore, when the concerned memory area can be changed).
- The management of tags in these communications is transparent and system-dependent. Therefore, they are never explicitly defined during calls to functions. An advantage of this is that collective communications never interfere with point-to-point communications.

---
<P class="chapter">Collective Communications</p>

## Types of collective communications

<h3 class="left">There are three types of subroutines :</h3>

1. One which ensures global synchronizations : `MPI_Barrier`
2. Ones which only transfer data :
   - Global distribution of data : `MPI_Bcast`
   - Selective distribution of data : `MPI_Scatter`
   - Collection of distributed data : `MPI_Gather`
   - Collection of distributed data by all the processes : `MPI_Allgather`
   - Collection and selective distribution by all the processes of distributed data : `MPI_Alltoall`
3. Ones which, in addition to the communications management, carry out operations on the transferred data :
   - Reduction operations (sum, product, maximum, minimum, etc.), whether of a predefined or personal type : `MPI_Reduce`
   - Reduction operations with distributing of the result (this is in fact equivalent to an `MPI_Reduce` followed by an `MPI_Bcast` : `MPI_Allreduce`

---
<P class="chapter">Collective Communications</p>

## MPI_Barrier

```
int MPI_Barrier(
  MPI_Comm comm
);

```

<img src="../figures/barrier.png" width="70%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Bcast

```c
int MPI_Bcast(void *buffer,int count,MPI_Datatype datatype,int root, MPI_Comm comm);
```

<img src="../figures/bcast.png" width="50%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Bcast

```c
int MPI_Bcast(void *buffer,int count,MPI_Datatype datatype,int root, MPI_Comm comm);
```
1. Send, starting at position `buffer`, a message of `count` element of type `datatype`, by the `root` process, to all the members of communicator `comm`.
2. .Receive this message at position buffer for all the processes other than the root.
---
<P class="chapter">Collective Communications</p>

## Exemple 5 : MPI_Bcast


```c
#include "mpi.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
    int rank value;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank==2){ value = rank+1000 }

    MPI_Bcast(&value,1,MPI_INT,2,MPI_COMM_WORLD);

    printf("Coucou, I process %d, I received %d from preocess 2 \n", rank,value);
    MPI_Finalize();
    return 0;
}
```

```bash
>mpiexec -n 4 ./exemple_05
Coucou, I process 2, I received 1002 from process 2
Coucou, I process 1, I received 1002 from process 2
Coucou, I process 3, I received 1002 from process 2
Coucou, I process 0, I received 1002 from process 2

```
---
<P class="chapter">Collective Communications</p>

## MPI_Scatter

```c
int MPI_Scatter(
    void *sendbuf,int sendcnt,MPI_Datatype sendtype,
    void *recvbuf,int recvcnt,MPI_Datatype recvtype,int root,
    MPI_Comm comm
);
```

<img src="../figures/scatter.png" width="40%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Scatter

```c
int MPI_Scatter(
    void *sendbuf,int sendcnt,MPI_Datatype sendtype,
    void *recvbuf,int recvcnt,MPI_Datatype recvtype,int root,
    MPI_Comm comm
);
```

1. Scatter by process `root`, starting at position `sendbuf`, message `sendcnt` element of type `sendtype`, to all the processes of communicator `comm`.
2. Receive this message at position `recvbuf`, of `recvcnt` element of type `recvtype` for all processes of communicator `comm`.

<h3 class="left">Remarks:</h3>

- The couples (`sendcnt`, `sendtype`) and (`recvcnt`, `recvtype`) must represent the same quantity of data.
- Data are scattered in chunks of same size ; a chunk consists of `sendcnt` elements of type `sendtype`.
- The i-th chunk is sent to the i-th process.
---
<P class="chapter">Collective Communications</p>

## Exemple 6 : MPI_Scatter


```c
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, blk_length, i;
    int nb_values = 8;
    float *recvdata, *values;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    blk_length = nb_values/num_proc;
    recvdata = malloc(blk_length * sizeof(float));
    if (rank==2){
    	// Initialisation of the matrix A on the process 0
    	values = malloc(nb_values*sizeof(float));
    	for(i=0;i<nb_values;i++){   values[i] = 1000+i ;}
    	printf("I, process %d send my array of %d values\n",rank,nb_values);
    }
    MPI_Scatter(values,blk_length,MPI_FLOAT,recvdata,blk_length,MPI_FLOAT,2,MPI_COMM_WORLD);

    printf("Coucou, I process %d, I received %5.1f %5.1f\n", rank,recvdata[0],recvdata[1]);
    MPI_Finalize();
    return 0;
}
```

```bash
>mpiexec -n 4 ./exemple_06
I, process 2 send my array of 8 values
Coucou, I process 1, I received  1002.0 1003.0
Coucou, I process 3, I received  1006.0 1007.0
Coucou, I process 2, I received  1004.0 1005.0
Coucou, I process 0, I received  1000.0 1001.0
```
---
<P class="chapter">Collective Communications</p>

## MPI_Gather

```c
int MPI_Gather(
  void *sendbuf,int sendcnt,MPI_Datatype sendtype,
  void *recvbuf,int recvcnt,MPI_Datatype recvtype,
  int root, MPI_Comm comm
);
```

<img src="../figures/gather.png" width="45%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Gather

```c
int MPI_Gather(
  void *sendbuf,int sendcnt,MPI_Datatype sendtype,
  void *recvbuf,int recvcnt,MPI_Datatype recvtype,
  int root, MPI_Comm comm
);
```
1. Send for each process of communicator `comm`, a message starting at position `sendbuf`, of `sendcnt` element type `sendtype`.
2. Collect all these messages by the `root` process at position `recvbuf`, `recvcnt` element of type `recvtype` .

<h3 class="left">Remark :</h3>

- The couples (`sendcnt`, `sendtype`) and (`recvcnt`, `recvtype`) must represent the same size of data.
- The data are collected in the order of the process ranks.


---
<P class="chapter">Collective Communications</p>

## Exemple 6 : MPI_Gather


```c
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, blk_length, i;
    int nb_values = 8;
    float *recvdata, *values;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    blk_length = nb_values/num_proc;
    values = malloc(blk_length*sizeof(float));

    printf("I, process %d send my array of %d values",rank,blk_length);
    for(i=0;i<blk_length;i++){
        values[i] = 1000+ rank*blk_length+i ;
        printf("%5.0f ",values[i]);} printf("\n");

    if (rank==2){recvdata = malloc(nb_values*sizeof(float));}

    MPI_Gather(values,blk_length,MPI_FLOAT,recvdata,blk_length, MPI_FLOAT,2,MPI_COMM_WORLD);

    if (rank==2){
        printf("I, process %d send my array of %d values",rank,blk_length);
        for(i=0;i<nb_values;i++){printf("%5.0f ",recvdata[i]);} printf("\n");
    }

    MPI_Finalize();
    return 0;
}
```


---
<P class="chapter">Collective Communications</p>

## Exemple 6 : MPI_Gather


```bash
>mpiexec -n 4 ./exemple_07
I, process 2 send my array of 2 values 1004  1005
I, process 0 send my array of 2 values 1000  1001
I, process 1 send my array of 2 values 1002  1003
I, process 3 send my array of 2 values 1006  1007
I, process 2 send my array of 2 values 1000  1001  1002  1003  1004  1005  1006  1007
```
---
<P class="chapter">Collective Communications</p>

## MPI_Allgather

```c
int MPI_Allgather(
  void *sendbuf, int sendcnt, MPI_Datatype sendtype,
  void *recvbuf, int recvcnt,  MPI_Datatype recvtype,
  MPI_Comm comm
);

```

<img src="../figures/allgather.png" width="40%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Allgather

```c
int MPI_Allgather(
  void *sendbuf, int sendcnt, MPI_Datatype sendtype,
  void *recvbuf, int recvcnt,  MPI_Datatype recvtype,
  MPI_Comm comm
);
```

1. Send by each process of communicator `comm`, a message starting at position `sendbuf`, of `sendcnt` element, type `sendtype` .
2. Collect all these messages, by all the processes, at position `recvbuf` of `recvcnt` element type `recvtype`.

<h3 class="left">Remarks:</h3>

- The couples (`sendcnt`, `sendtype`) and (`recvcnt`, `recvtype`) must represent the same data size.
- The data are gathered in the order of the process ranks

---
<P class="chapter">Collective Communications</p>

## Exemple 6 : MPI_Allgather


```c
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, blk_length, i;
    int nb_values = 8;
    float *recvdata, *values;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    blk_length = nb_values/num_proc;
    values = malloc(blk_length*sizeof(float));
    recvdata = malloc(nb_values*sizeof(float));

    printf("I, process %d send my array of %d values",rank,blk_length);
    for(i=0;i<blk_length;i++){ values[i] = 1000+ rank*blk_length+i;}

    MPI_Allgather(values,blk_length,MPI_FLOAT,recvdata,blk_length,
    		MPI_FLOAT,MPI_COMM_WORLD);

    printf("I, process %d received ",rank);
    for(i=0;i<nb_values;i++){printf("%5.0f ",recvdata[i]);}
    printf("\n");

    MPI_Finalize();
    return 0;
}
```

---
<P class="chapter">Collective Communications</p>

## Exemple 6 : MPI_Allgather


```bash
>mpiexec -n 4 ./exemple_08
I, process 0 send my array of 2 valuesI, process 0 received  1000  1001  1002  1003  1004  1005  1006  1007
I, process 1 send my array of 2 valuesI, process 1 received  1000  1001  1002  1003  1004  1005  1006  1007
I, process 2 send my array of 2 valuesI, process 2 received  1000  1001  1002  1003  1004  1005  1006  1007
I, process 3 send my array of 2 valuesI, process 3 received  1000  1001  1002  1003  1004  1005  1006  1007
```
---
<P class="chapter">Collective Communications</p>

## MPI_Gatherv

```c
int MPI_Gatherv(
  void *sendbuf, int sendcnt,MPI_Datatype sendtype,
  void *recvbuf, int *recvcnts, int *displs, MPI_Datatype recvtype,
  int root, MPI_Comm comm
);
```
<img src="../figures/gatherv.png" width="40%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Gatherv

```c
int MPI_Gatherv(
  void *sendbuf, int sendcnt, MPI_Datatype sendtype,
  void *recvbuf, int *recvcnts, int *displs, MPI_Datatype recvtype,
  int root, MPI_Comm comm
);
```

The i-th process of the communicator `comm` sends to process `root`, a message starting at position `sendbuf`, of `sendcnt` elements of type `sendtype`, and receives at position `recvbuf`, of `recvcnts[i]` elements of type `recvtype`, with a displacement of `displs[i]`.

<h3 class="left">Remarks:</h3>

- The couples (`sendcnt`,`sendtype`) of the i-th process and (`recvcnt(i)`, `recvtype`) of process root must be such that the data size sent and received is the same.
---
<P class="chapter">Collective Communications</p>

## Exemple 9 : MPI_Gatherv


```c
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
int rank, num_proc, blk_length, i, remainder;
int nb_values = 10;
float *recvdata, *values;
int *displacement, *nb_received;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
    blk_length = nb_values/num_proc;
    remainder = nb_values%num_proc;
    if(rank<remainder) blk_length++;
    values = malloc(blk_length*sizeof(float));
    printf("I, process %d send my array of %d values %d",rank,blk_length,remainder);
    for(i=0;i<blk_length;i++){
        values[i] = 1000+ rank*(blk_length)+i ;
        printf("%5.0f ",values[i]);} printf("\n");
    if (rank==2){
        recvdata = malloc(nb_values*sizeof(float));
        nb_received = malloc(num_proc*sizeof(int));
        displacement = malloc(num_proc*sizeof(int));
        nb_received[0] = nb_values/num_proc;
        if(remainder>0) nb_received[0]++;
        displacement[0] = 0;
        for(i=1;i<num_proc;i++){
            displacement[i] = displacement[i-1]+nb_received[i-1];
            nb_received[i] = nb_values/num_proc;
            if(i<remainder) nb_received[i]++;
        }
   }
   MPI_Gatherv(values,blk_length,MPI_FLOAT,recvdata,nb_received,displacement,MPI_FLOAT,2,MPI_COMM_WORLD);
```

---
<P class="chapter">Collective Communications</p>

## Exemple 9 : MPI_Gatherv


```
   if (rank==2){
        printf("I, process %d receive an array of %d values",rank,blk_length);
        for(i=0;i<nb_values;i++){printf("%5.0f ",recvdata[i]);}
        printf("\n");
   }

    MPI_Finalize();
    return 0;
}
```

```bash
>mpiexec -n 4 ./exemple_09
I, process 1 send my array of 3 values 2 1003  1004  1005
I, process 0 send my array of 3 values 2 1000  1001  1002
I, process 2 send my array of 2 values 2 1004  1005
I, process 3 send my array of 2 values 2 1006  1007
I, process 2 receive an array of 2 values 1000  1001  1002  1003  1004  1005  1004  1005  1006  1007
```
---
<P class="chapter">Collective Communications</p>

## MPI_Alltoall

```c
int MPI_Alltoall(
  void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount, MPI_Datatype recvtype,
  MPI_Comm comm
  );
```

<img src="../figures/alltoall.png" width="40%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Alltoall

```c
int MPI_Alltoall(
  void *sendbuf, int sendcount, MPI_Datatype sendtype,
  void *recvbuf, int recvcount, MPI_Datatype recvtype,
  MPI_Comm comm
  );
```

Here, the i-th process sends its j-th chunk to the j-th process which places it in its i-th chunk.

<h3 class="left">Remark:</h3>

- The couples (`sendcount`, `sendtype`) and (`recvcount`, `recvtype`) must be such that they represent equal data sizes.


---
<P class="chapter">Collective Communications</p>

## Exemple 9 : MPI_Alltoall


```c
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, i, blk_length, nb_value = 8;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    float *values = malloc(nb_value*sizeof(float));
    float *recvdata = malloc(nb_value*sizeof(float));
    blk_length = nb_value/num_proc;

    printf("I, process %d send my array :",rank);
    for(i=0;i<nb_value;i++){values[i] = 1000.+rank*nb_value+i;  printf("%5.0f ",values[i]);} printf("\n");

    MPI_Alltoall(values,blk_length,MPI_FLOAT,recvdata,blk_length,MPI_FLOAT,MPI_COMM_WORLD);

    printf("I, process %d received array : ",rank);
    for(i=0;i<nb_value;i++){ printf("%5.0f ",recvdata[i]);} printf("\n");

    MPI_Finalize();   return 0;
}

```
```bash
>mpiexec -n 4 ./exemple_10
I, process 0 send my array : 1000  1001  1002  1003  1004  1005  1006  1007
I, process 1 send my array : 1008  1009  1010  1011  1012  1013  1014  1015
I, process 2 send my array : 1016  1017  1018  1019  1020  1021  1022  1023
I, process 3 send my array : 1024  1025  1026  1027  1028  1029  1030  1031
I, process 0 received array :  1000  1001  1008  1009  1016  1017  1024  1025
I, process 1 received array :  1002  1003  1010  1011  1018  1019  1026  1027
I, process 2 received array :  1004  1005  1012  1013  1020  1021  1028  1029
I, process 3 received array :  1006  1007  1014  1015  1022  1023  1030  1031
```
---
<P class="chapter">Collective Communications</p>

## TP 2.5 : MPI_Alltoall


- Write a program that performs the transpose of a square matrix $A$ of arbitrary dimension (multiple of the number of processus).

- The matrix is split among the processus and initialized so that each element is unique (use row-column number and processus rank).

- Thus you need to evaluate B of the form:

$$B = A^T$$

<img src="../figures/tp_2.5_transpose.png" width="60%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## TP 2.5 : MPI_Alltoall

$$B = A^T$$

<img src="../figures/transpose_block.png" width="40%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## Reduction


<h3 class="left">Global reduction</h3>


- A reduction is an operation applied to a set of elements in order to obtain one single value.
Typical examples are the sum of the elements of a vector (`SUM(A[*])`) or the search for the maximum value element in a vector (`MAX(V[*])`).
- MPI proposes high-level subroutines in order to operate reductions on data distributed on a group of processes. The result is obtained on only one process (`MPI_Reduce()`) or on all the processes (`MPI_Allreduce()`, which is in fact equivalent to an `MPI_Reduce()` followed by an `MPI_Bcast()`).
- If several elements are implied by process, the reduction function is applied to each one of them (for instance to each element of a vector).
---
<P class="chapter">Collective Communications</p>

## MPI_Reduce

```c
int MPI_Reduce(
  void *sendbuf, void *recvbuf, int count,
  MPI_Datatype datatype, MPI_Op op,
  int root, MPI_Comm comm
);

```

<img src="../figures/reduction.png" width="60%" style="justify-content: center"></img>

---
<P class="chapter">Collective Communications</p>

## MPI_Reduce

```c
int MPI_Reduce(
  void *sendbuf, void *recvbuf, int count,
  MPI_Datatype datatype, MPI_Op op,
  int root, MPI_Comm comm
);

```

1. Distributed reduction of `count` elements of type `datatype`, starting at position `sendbuf`, with the operation `op` from each process of the communicator `comm`,
2. Return the result at position `recvbuf` in the process `root`


---
<P class="chapter">Collective Communications</p>

## MPI_Reduce Operations

|Name |Operation |
| :------------- | :------------- |
|MPI_SUM |Sum of element          |
|MPI_PROD| Product of elements|
|MPI_MAX | Maximum of elements|
|MPI_MIN |Minimum of elements|
|MPI_MAXLOC| Maximum of elements and location|
|MPI_MINLOC| Minimum of elements and location|
|MPI_LAND| Logical AND |
|MPI_LOR |Logical OR|
|MPI_LXOR| Logical exclusive OR|
---
<P class="chapter">Collective Communications</p>

## Exemple 9 : MPI_Reduce


```c
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, i, value, sum;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    if(rank==0){
	    value=1000;
    }
    else{
	    value=rank;
    }

    MPI_Reduce(&value,&sum,1,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);

    if(rank==0){
	   printf("I, process %d have global sum value : %d ",rank,sum);
    }

    MPI_Finalize();
    return 0;
}
```

```bash
>mpiexec -n 4 ./exemple_11
I, process 0 have global sum value : 1006
```
---
<P class="chapter">Collective Communications</p>

## MPI_Allreduce

```c
int MPI_Allreduce(
  void *sendbuf,
  void *recvbuf,
  int count,
  MPI_Datatype datatype,
  MPI_Op op,
  MPI_Comm comm
);

```

<img src="../figures/allreduce.png" width="50%" style="justify-content: center"></img>


---
<P class="chapter">Collective Communications</p>

## MPI_Allreduce

```c
int MPI_Allreduce(
  void *sendbuf,
  void *recvbuf,
  int count,
  MPI_Datatype datatype,
  MPI_Op op,
  MPI_Comm comm
);

```
1. Distributed reduction of `count` elements of type `datatype` starting at position `sendbuf`, with the operation `op` from each process of the communicator `comm`.

2. Write the result at position `recvbuf` for all the processes of the communicator `comm`.


---
<P class="chapter">Collective Communications</p>

## Exemple 9 : MPI_AllReduce


```c
#include "mpi.h"
#include <stdio.h>
int main(int argc, char *argv[]) {
    int rank, num_proc, i, value, sum;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

    if(rank==0){
    	value=1000;
    }
    else{
	    value=rank;
    }

    MPI_Allreduce(&value,&sum,1,MPI_INT,MPI_PROD,MPI_COMM_WORLD);

    printf("I, process %d have global sum value : %d \n",rank,sum);

    MPI_Finalize();
    return 0;
}
```

```bash
>mpiexec -n 4 ./exemple_12
I, process 0 have global sum value : 6000
I, process 1 have global sum value : 6000
I, process 2 have global sum value : 6000
I, process 3 have global sum value : 6000

```
---
<P class="chapter">Collective Communications</p>

## .... And others communications

- In reductions : The `MPI_Op_create()` and `MPI_Op_free()` subroutines allow to create personal reduction operations.
- In reduction the keyword `MPI_IN_PLACE` can be used in order to keep the result in the same place as the sending buffer (but only for the rank(s)that will receive results).
Example : `MPI_Allreduce(MPI_IN_PLACE,sendrecvbuf,...)`

- Similarly to what we have seen for `MPI_Gatherv()` with repect to `MPI_Gather()`, the `MPI_Scatterv()`, `MPI_Allgatherv()` and `MPI_Alltoallv()` subroutines extend `MPI_Scatter()`, `MPI_Allgather()` and `MPI_Alltoall()` to the cases where the processes have different numbers of elements to transmit or gather.
---
<P class="chapter">Collective Communications</p>

## Exercise 3 : Calculation of $\pi$


- The aim of this exercice is to compute $\pi$ by numerical integration: $\pi=\int_0^1 1/(1+x^2)dx$.
- We use the rectangle method (mean point).
- Let $f(x) =4/(1+x^2)$ be the function to integrate.
- $nbblock$ is the number of points of discretization.
- $width=1/nbblock$ the length of discretization and the width of all rectangles.
- Sequential version is available in the `pi.c` source file.
- You have to do the parallel version with MPI in this file.
---
<P class="chapter">Collective Communications</p>

## Exercise 3.5 : Reduction


- Each process initializes a one-dimensional array by giving to all the elements the value of its rank+1.
- Then the root process (task 0) performs two reduce operations (sum and then product) to the arrays of all the processes. - Finally, each process generates a random number and root process finds (and prints) the maximum value among these random values.

- Modify the code to perform a simple scalability test using MPI_Wtime. Notice what happens when you go up with the number of processes involved.
