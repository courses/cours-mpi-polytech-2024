<p class="chapter">Communicators</p>

# Communicators

-----
<p class="chapter">Communicators</p>

### Introduction
The purpose of communicators is to create subgroups on which we can carry out
operations such as collective or point-to-point communications. Each subgroup will
have its own communication space.

<img src="../figures/mpi_comme_world.png" width="30%" ></img>



-----
<p class="chapter">Communicators</p>

<h3 class="left2">Example</h3>

For example we want to broadcast a collective message to even-ranked processes and another message to odd-ranked processes.
- Looping on send/recv can be very detrimental especially if the number of processes is high. Also a test inside the loop would be compulsory in order to know if the sending process must send the message to an even or odd process rank.
- A solution is to create a communicator containing the even-ranked processes,another containing the odd-ranked processes, and initiate the collective communications inside these groups.



-----
<p class="chapter">Communicators</p>

### Default communicator

- A communicator can only be created from another communicator. The first one will be created from the `MPI_COMM_WORLD`.
- After the `MPI_Init` call, a communicator is created for the duration of the program execution.
Its identifier `MPI_COMM_WORLD` is an integer value defined in the header files.
- This communicator can only be destroyed via a call to `MPI_Finalize`.
- By default, therefore, it sets **the scope** of collective and point-to-point communications **to include all the processes of the application**.



-----
<p class="chapter">Communicators</p>

### Groups and communicators

- A communicator consists of :
    - A **group**, which is an ordered group of processes.
    - A communication **context** put in place by calling one of the communicator construction subroutines, which allows determination of the communication space.
- The communication contexts are managed by MPI (the programmer has no action on them : It is a hidden attribute).
- In the MPI library, the following subroutines exist for the purpose of building communicators: `MPI_Comm_create`, `MPI_Comm_dup`, `MPI_Comm_split`
- The **communicator constructors** are **collective calls**.
- Communicators created by the programmer can be destroyed by using the `MPI_Comm_free` subroutine.



-----
<p class="chapter">Communicators</p>

### Partitioning of a communicator
In order to solve the previous example :
- Make a partition of the communicator into odd-ranked and even-ranked processes.
- Broadcast a message inside the odd-ranked processes and another message inside the even-ranked processes.

<img src="../figures/odd_even_comm.png" width="40%" ></img>



-----
<p class="chapter">Communicators</p>

### Communicators Partitioning of a communicator with `MPI_Comm_split`
The `MPI_Comm_split` allows :
- Partitioning a given communicator into as many communicators as we want.
- Giving the same name to all these communicators where the process value will be the value of its communicator.
- Method :
    1. Define a `color` value for each process, associated with its communicator number.
    2. Define a `key` value for ordering the processes in each communicator.
    3. Create the partition where each communicator is called `new_comm`.

```C
int MPI_Comm_split(
    MPI_Comm comm,
    int color,
    int key,
    MPI_Comm *new_comm
);
```
A process which assigns a color value equal to `MPI_UNDEFINED` will have the invalid communicator `MPI_COMM_NULL` for `new_comm`.




-----
<p class="chapter">Communicators</p>

### Communicators Partitioning of a communicator with `MPI_Comm_split` : Example 17

Let’s look at how to proceed in order to build the communicator which will subdivide the communication space into odd-ranked and even-ranked processes via the `MPI_comm_split` constructor.

<img src="../figures/rank_even.png" width="80%" ></img>


-----
<p class="chapter">Communicators</p>

### Communicators Partitioning of a communicator with `MPI_Comm_split` : Example 17

```C
int main(int argc, char* argv[])
{
    int m=4;
    int w_rank;
    float a[m];
    MPI_Comm CommEvenOdd;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);

    for(int i=0;i<m;i++){a[i] = 0;}
    if(w_rank==2) for(int i=0;i<m;i++){	a[i] = 2;}
    if(w_rank==5) for(int i=0;i<m;i++){	a[i] = 5;}

    int key = w_rank;
    if(w_rank==2 || w_rank==5){key=-1;};

    /* Creation of even and odd communicators by giving them the same name */
    MPI_Comm_split(MPI_COMM_WORLD,w_rank%2,key,&CommEvenOdd);

    /* Broadcast of the message by the rank process 0 of each communicator to the processes of its group */
    MPI_Bcast(&a,m,MPI_REAL,0,CommEvenOdd);

    printf("%d %f\n",w_rank,a[0]);

    MPI_Comm_free(&CommEvenOdd);
    MPI_Finalize();
    return 0;
}

```


-----
<p class="chapter">Communicators</p>


### Communicator built from a group

- We can also build a communicator by defining a group of processes :
Call to `MPI_Comm_group`, `MPI_Group_incl`, `MPI_Comm_create`, `MPI_Group_free` .
- This process is however far more cumbersome than using `MPI_Comm_split` whenever possible.


-----
<p class="chapter">Communicators</p>

### Topologies

In most applications, specially in domain decomposition methods where we match the calculation domain to the process grid, it is helpful to be able to arrange the processes according to a regular topology.
- MPI allows to define virtual cartesian or graph topologies.
  - Cartesian topologies :
    - Each process is defined in a grid.
    - Each process has a neighbour in the grid.
    - The grid can be periodic or not.
    - The processes are identified by their coordinates in the grid.
  - Graph topologies : It Can be used in more complex topologies.
    - Can be used in more complex topologies.



-----
<p class="chapter">Communicators</p>

### Cartesian topologies
- A Cartesian topology is defined from a given communicator named `comm_old`, calling the `MPI_Cart_create` subroutine.
- We define :
    - An integer `ndims` representing the number of grid dimensions.
    - An integer array `dims` of dimension `ndims` showing the number of processes in each dimension.
    - A periods array of `ndims` int, which shows the periodicity of each dimension.
    - A logical reorder which shows if the process numbering can be changed by MPI

```C
int MPI_Cart_create(
    MPI_Comm comm_old,
    int ndims,
    int *dims,
    int *periods,
    int reorder,
    MPI_Comm *comm_cart
);
```


-----
<p class="chapter">Communicators</p>

### Example 18 : on a grid having 4 domains along x and 2 along y, periodic in y

```
int main(int argc, char *argv[]) {
    MPI_Comm comm_2D;
    int ndims = 2;
    int dims[2] = {4, 2}; // dims[0]*dims[1] = nb_processus

    MPI_Init(&argc, &argv);

    int periods[2] = {0,1};
    int reorder = 0;

    MPI_Cart_create(MPI_COMM_WORLD,ndims,dims,periods,reorder,&comm_2D);

    MPI_Finalize();
    return 0;
}
```
if `reorder = 0` then the rank of the processes in the new communicator (`comm_2D`) is the same as in the old communicator (`MPI_COMM_WORLD`).

If `reorder != 0`, the MPI implementation chooses the order of the processes.


-----
<p class="chapter">Communicators</p>


### Example 18 : on a grid having 4 domains along x and 2 along y, periodic in y

<img src="../figures/cart_2D.png" width="45%" ></img>



-----
<p class="chapter">Communicators</p>

### Example : n a 3D grid having 4 domains along x, 2 along y and 2 along z, non periodic.

```
int main(int argc, char *argv[]) {
    MPI_Comm comm_3D;
    int ndims = 3;
    int dims[3] = {4,2,2}; // dims[0]*dims[1]*dims[2] = nb_processus

    MPI_Init(&argc, &argv);

    int periods[2] = {0,0,0};
    int reorder = 0;

    MPI_Cart_create(MPI_COMM_WORLD,ndims,dims,periods,reorder,&comm_3D);

    MPI_Finalize();
    return 0;
}
```


-----
<p class="chapter">Communicators</p>

### Example : a 3D grid having 4 domains along x, 2 along y and 2 along z, non periodic.

<img src="../figures/cart_3D.png" width="45%" ></img>


-----
<p class="chapter">Communicators</p>


<h3 class="left2"> Rank of a process</h3>

In a Cartesian topology, the function `MPI_Cart_rank` gives the rank of the process associated to the coordinates in the grid.

```C
int MPI_Cart_rank(
    MPI_Comm comm,
    int *coords,  // In
    int *rank    //  Out
);
```
<spacer type="horizontal" width="10" height="5"> _</spacer>

<h3 class="left2"> Coordinates of a process</h3>

In a cartesian topology, the function `MPI_CART_COORDS` gives the coordinates of a process of a given rank in the grid.

```C
int MPI_Cart_coords(
    MPI_Comm comm,
    int rank,    // In
    int maxdims, // In
    int *coords  // Out
);
```


-----
<p class="chapter">Communicators</p>

### Process distribution
The `MPI_Dims_create` subroutine returns the number of processes in eachdimension of the grid according to the total number of processes.

```C
int MPI_Dims_create(
    int nnodes,
    int ndims,
    int *dims
);
```
Remark : If the values of dims in entry are all 0, then we leave to MPI the choice of thenumber of processes in each direction according to the total number of processes.


-----
<p class="chapter">Communicators</p>

### Example 19

```C
int main(int argc, char *argv[]) {
    MPI_Comm comm_3D;
    int ndims = 3, reorder = 0;
    int periods[3] = {0,0,0};
    int coord[3] ,rank, nproc;
    int dims[3] = {0,0,0}; // dims[0]*dims[1]*dims[2] = nb_processus

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    MPI_Dims_create(nproc,ndims,dims);
    if(rank==0) printf("Created dims is %d %d %d\n\n",dims[0],dims[1],dims[2]);

    MPI_Cart_create(MPI_COMM_WORLD,ndims,dims,periods,reorder,&comm_3D);

    MPI_Finalize();
    return 0;
}
```

```sh
> mpiexec -n 16 ./exemple_19
> Created dims is 4 2 2
```


-----
<p class="chapter">Communicators</p>

### Rank of neighbours
In a Cartesian topology, a process that calls the `MPI_Cart_shift` subroutine can obtain the rank of a neighboring process in a given direction.

```C
int MPI_Cart_shift(
  MPI_Comm comm,
  int direction,
  int step,
  int *rank_prev,
  int *rank_next
);
```
- The direction parameter corresponds to the displacement axis (xyz).
- The step parameter corresponds to the displacement step.
- If a rank does not have a neighbor before (or after) in the requested direction, then the value of the previous (or following) rank will be MPI_PROC_NULL.


-----
<p class="chapter">Communicators</p>

### Rank of neighbours
<img src="../figures/shift.png" width="45%" ></img>

```C
MPI_Cart_shift(comm_2D,0,1,&rank_left, &rank_right);
// For the process 2, rank_left=0, rank_right=4
```

```C
MPI_Cart_shift(comm_2D,1,1,&rank_low, &rank_high)
// For the process 2, rank_low=3, rank_high=3
```

-----
<p class="chapter">Communicators</p>

### Exemple 20 : Domain decomposition

```C
int main(int argc, char *argv[]) {
    MPI_Comm comm_2D;
    int ndims = 2, reorder = 0;
    int periods[2], coord[2], dims[2]={0,0};
    int rank, nproc;
    const int N=0, E=1, S=2, W=3;
    int neighbor[4];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nproc);

    // Know the number of processes along x and y
    MPI_Dims_create(nproc,ndims,dims);

    // 2D y-periodic grid creation
    periods[0] = 0; periods[1] = 1;

    MPI_Cart_create(MPI_COMM_WORLD,ndims,dims,periods,reorder,&comm_2D);
    // Know my coordinates in the topology
    MPI_Comm_rank(comm_2D,&rank);
    MPI_Cart_coords(comm_2D,rank,ndims,coord);
```


-----
<p class="chapter">Communicators</p>

### Exemple 20 : Domain decomposition
```C
    // Search of my West and East neigbors
    MPI_Cart_shift(comm_2D,0,1,&neighbor[W],&neighbor[E]);
    // Search of my South and North neighbors
    MPI_Cart_shift(comm_2D,1,1,&neighbor[S],&neighbor[N]);

    printf("rank: %d of coords (%d,%d) has neighbors : %d %d %d %d\n",
	   rank,coord[0],coord[1],neighbor[N],neighbor[S],neighbor[E],neighbor[W]);

    MPI_Finalize();
    return 0;
}
```

```bash
> mpiexec  -n 8 ./exemple_20
rank: 0 of coords (0,0) has neighbors : 1 1 2 -2
rank: 1 of coords (0,1) has neighbors : 0 0 3 -2
rank: 2 of coords (1,0) has neighbors : 3 3 4 0
rank: 3 of coords (1,1) has neighbors : 2 2 5 1
rank: 4 of coords (2,0) has neighbors : 5 5 6 2
rank: 5 of coords (2,1) has neighbors : 4 4 7 3
rank: 6 of coords (3,0) has neighbors : 7 7 -2 4
rank: 7 of coords (3,1) has neighbors : 6 6 -2 5
```
